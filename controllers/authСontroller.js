const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
require('dotenv').config();
const { ClientErrorHandler, ServerErrorHandler } = require('../controllers/errorController');
const SECRET = process.env.SECRET;

//
// const ClientErrorHandler = (res, error) => {
//     console.log(error);
//     res.status(400).send({message: 'Some client error'});
// };
//
// const ServerErrorHandler = (res, error) => {
//     console.log(error);
//     res.status(500).send({message: 'Some server error'});
// };

const generateAccessToken = (id, username) => {
    const payload = { id, username };
    return jwt.sign(payload, SECRET, { expiresIn: '7d' });
};


const registerUser = (req, res) => {
    // const {username, password} = req.body;
    try {
        const { username, password } = req.body;
        const hashPassword = bcrypt.hashSync(password, 5);
        const user = new User({ username, password: hashPassword });
        user.save().then(() => res.status(200).send({ message: 'Success' }))
            .catch((error) => ClientErrorHandler(res, error));
    } catch (error) {
        ServerErrorHandler(res, error);
    }
};

const loginUser = async(req, res) => {
    try {
        const { username, password } = req.body;
        const user = await User.findOne({ username });
        if (!user) {
            return res.status(400).send({ message: 'some user error' });
        }
        const validPassword = bcrypt.compare(password, user.password);
        if (!validPassword) {
            return res.status(400).send({ message: 'invalid user password' });
        }
        const token = generateAccessToken(user._id, user.username);
        return res.status(200).send({ message: 'Success', jwt_token: token });
    } catch (error) {
        ServerErrorHandler(res, error);
    }
};

module.exports = { registerUser, loginUser };