const User = require('../models/user');
const { ServerErrorHandler, ClientErrorHandler } = require('../controllers/errorController');


const changePassword = async(req, res) => {
    try {
        const hashedPassword = bcrypt.hashSync(req.body.newPassword, 5);
        User.findByIdAndUpdate(req.user.id, { password: hashedPassword })
            .then(() => res.status(200).send({ message: 'Success' }))
            .catch((error) => ClientErrorHandler(res, error));
    } catch (error) {
        res.status(200).send({ message: 'Success' });
    }
};

const getUserInfo = async(req, res) => {
    try {
        const user = await User.findOne({ username: req.user.username });
        const { _id, username, createdDate } = user;
        return res.status(200).send({
            user: { _id, username, createdDate }
        });
    } catch (error) { ServerErrorHandler(res, error) }
};

const deleteUser = async(req, res) => {
    try {
        User.findByIdAndDelete(req.user.id)
            .then(() => res.status(200).send({ message: 'Success' }))
            .catch((error) => handleClientError(res, error));
    } catch (error) {
        ServerErrorHandler(res, error);
    }
};




module.exports = { getUserInfo, deleteUser, changePassword };