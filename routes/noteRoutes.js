const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');
const { createNote, getNote, getNotes, deleteNote, checkNote, updateNote } = require('../controllers/noteСontroller');

router.post('/api/notes', authMiddleware, createNote);
router.patch('/api/notes/:id', authMiddleware, checkNote);
router.get('/api/notes/:id', authMiddleware, getNote);
router.get('/api/notes', authMiddleware, getNotes);
router.delete('/api/notes/:id', authMiddleware, deleteNote);
router.put('/api/notes/:id', authMiddleware, updateNote);


module.exports = router;