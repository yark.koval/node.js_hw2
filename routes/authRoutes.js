const express = require('express');
const {loginUser, registerUser} = require('../controllers/authСontroller');
const router = express.Router();

router.post('/api/auth/login', loginUser);
router.post('/api/auth/register', registerUser);

module.exports = router;