const express = require('express');
const { getUserInfo, deleteUser, changePassword } = require('../controllers/userСontroller');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');

router.patch('/api/users/me', authMiddleware, changePassword);
router.get('/api/users/me', authMiddleware, getUserInfo);
router.delete('/api/users/me', authMiddleware, deleteUser);
module.exports = router;